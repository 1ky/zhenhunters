public class Zhen extends GamePiece implements ControlActions{
    private int[] newPos = {0,0};

    public Zhen(){
        setType("zhen");
        setSymbol('Z');
    }

    public void setNewPos(int x, int y) {
        this.newPos[0] = getRowPos() +x;
        this.newPos[1] = getColPos() +y;
    }

    int[] getNewPos()
    {
        return newPos;
    }
    //Moves
    public int[] findNewPos(int direction) {
        final int North = 1;
        final int South = 2;
        final int East = 3;
        final int West = 4;
        final int NorthWest = 5;
        final int SouthWest = 6;
        final int SouthEast = 7;
        final int NorthEast = 8;

        if (direction == North) {
            setNewPos(-1, 0);
            return getNewPos();
        }
        if (direction == South) {
            setNewPos(1, 0);
            return getNewPos();
        }
        if (direction == East) {
            setNewPos(0, 1);
            return getNewPos();
        }
        if (direction == West) {
            setNewPos(0, -1);
            return getNewPos();
        }
        if (direction == NorthWest) {
            setNewPos(-1, -1);
            return getNewPos();
        }
        if (direction == SouthWest) {
            setNewPos(1, -1);
            return getNewPos();
        }
        if (direction == SouthEast) {
            setNewPos(+1, +1);
            return getNewPos();
        }
        if (direction == NorthEast) {
            setNewPos(-1, 1);
            return getNewPos();
        }
        return getNewPos();
    }

    public void moveToNewPos(int direction) {
        int[] dir = findNewPos(direction);
        System.out.println("We are moving to " + dir[0]+ " " +dir[1]);

        setRowPos(getNewPos()[0]);
        setColPos(getNewPos()[1]);
    }


}
