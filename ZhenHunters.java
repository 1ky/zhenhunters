import java.util.Scanner;

public class ZhenHunters {
    public static void main(String args[]){

        //Welcome the users to the game
        System.out.println("Welcome to ZhenHunters");
        Scanner myScanner = new Scanner(System.in);

        //Creates grid
        Gamegrid game = new Gamegrid();
        game.setupGrid();
        game.displayGrid();

        int r1, c1;
        int r2, c2;
        int d;

        while (game.getNumZhens() > 0) {
            System.out.println("\nSelect the Zhen you want to move.");
            System.out.println("Enter the Row:");
            r1 = myScanner.nextInt();
            System.out.println("Enter the Column");
            c1 = myScanner.nextInt();

            GamePiece z;
            GamePiece h;

            while (!((z = game.getGrid(r1,c1)) instanceof Zhen)) {
                System.out.println("That was not a Zhen, select again");
                System.out.println("Enter the Row:");
                r1 = myScanner.nextInt();
                System.out.println("Enter the Column");
                c1 = myScanner.nextInt();
            }

            game.displayMovementOptions(2);
            System.out.println("Select a direction");
            d = myScanner.nextInt();

            if (z instanceof Zhen) {
                System.out.println(z.getRowPos() + " - " + z.getColPos());
                ((Zhen) z).moveToNewPos(d);
                System.out.println(z.getRowPos() + " - " + z.getColPos());
            }

            game.setGrid(z.getRowPos(),z.getColPos(),z);
            game.setGrid(r1,c1,new GamePiece());
            game.displayGrid();

            System.out.println("\nSelect the Hunter you want to move.");
            System.out.println("Enter the Row:");
            r2 = myScanner.nextInt();
            System.out.println("Enter the Column");
            c2 = myScanner.nextInt();

            while (!((h = game.getGrid(r2,c2)) instanceof Hunter)) {
                System.out.println("That was not a Hunter, select again");
                System.out.println("Enter the Row:");
                r2 = myScanner.nextInt();
                System.out.println("Enter the Column");
                c2 = myScanner.nextInt();
            }

            game.displayMovementOptions(1);
            System.out.println("Select a direction");
            d = myScanner.nextInt();

            if (h instanceof Hunter) {
                System.out.println(h.getRowPos() + " - " + h.getColPos());
                ((Hunter) h).moveToNewPos(d);
                System.out.println(h.getRowPos() + " - " + h.getColPos());
            }

            game.setGrid(h.getRowPos(),h.getColPos(),h);
            game.setGrid(r2,c2,new GamePiece());
            game.displayGrid();
        }
    }
}
