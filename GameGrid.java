import java.util.Scanner;
public class Gamegrid {
    private int numRows;
    private int numCols;
    private GamePiece[][] grid;
    private Player player1, player2;
    private int numZhens;
    private int[] hunter1Coord;
    private int[] hunter2Coord;
    private int numHunters;

    //constructor Gamegrid
    public Gamegrid() {
        setNumRows(8);
        setNumCols(8);
        grid = new GamePiece[getNumRows()][getNumCols()];
    }

    //set hunter coordinates 1
    public void setHunter1Coord(int[] rc) {
        hunter1Coord = rc;
    }

    //set hunter coordinates 2
    public void setHunter2Coord(int[] rc) {
        hunter2Coord = rc;
    }

    //set grid
    public void setGrid(int row, int col, GamePiece grid) {
        this.grid[row][col] = grid;
    }

    //set number of rows
    public void setNumRows(int nRows) {
        numRows = nRows;
    }

    //set number of columns
    public void setNumCols(int nCols) {
        numCols = nCols;
    }

    //set number of Zhens
    public void setNumZhens(int nZhens) {
        numZhens = nZhens;
    }

    //set number of Hunters
    public void setNumHunters(int nHunters) {
        numHunters = nHunters;
    }

    //get Grid
    public GamePiece getGrid(int row, int col) {
        return grid[row][col];
    }

    //get number of columns
    public int getNumCols() {
        return numCols;
    }

    //get number of Hunters
    public int getNumHunters() {
        return numHunters;
    }

    //get number of rows
    public int getNumRows() {
        return numRows;
    }

    //get number of Zhens
    public int getNumZhens() {
        return numZhens;
    }

    //get hunter coordinates 1
    public int[] getHunter1Coord() {
        return hunter1Coord;
    }

    //get number coordinates 2
    public int[] getHunter2Coord(){
        return hunter2Coord;
    }

    //method used to add the piece to the grid respresented by rows and columns
    boolean addGamePiece(GamePiece piece, int row, int col) {
        if (getGrid(row,col).getSymbol() == '?') {
            setGrid(row,col, piece);
            return true;
        }
        return false;
    }

    //Set up grid
    public void setupGrid() {
        setNumHunters(2);
        Scanner scanner = new Scanner(System.in);
        Scanner scannerInt = new Scanner(System.in);
        Scanner scanHunt = new Scanner(System.in);
        //ask the user to enter name for player 1
        System.out.println("Player 1 enter your name");
        player1 = new Player(scanner.next());
        //ask the user to enter name for player 2
        System.out.println("Player 2 enter your name");
        player2 = new Player(scanner.next());
        //ask the user how many Zhens they want to play with
        System.out.println("How many Zhens do you want to play with?(16/18/20)");
        int z = scannerInt.nextInt();
        setNumZhens(z);
        //sets coordinates for Hunters
        int x[]={4,3};
        int y[]={4,4};
        setHunter1Coord(x);
        setHunter2Coord(y);
        Hunter h1 = new Hunter();
        Hunter h2 = new Hunter();
        h1.setRowPos(x[0]);
        h1.setColPos(x[1]);
        h2.setRowPos(y[0]);
        h2.setColPos(y[1]);
        //ask the user to enter a character to represent Hunter1
        System.out.println("Enter a Character for Hunter1");
        String h = scanHunt.next();
        h1.setSymbol(h.charAt(0));
        //ask the user to enter a character to represent Hunter2
        System.out.println("Enter a Character for Hunter2");
        String r = scanHunt.next();
        h2.setSymbol(r.charAt(0));
        while ((getNumZhens() != 16) && (getNumZhens() != 18) && (getNumZhens() != 20)){
            System.out.println("not a valid number, How many Zhens do you want to play with?(16/18/20)");
            z = scannerInt.nextInt();
            setNumZhens(z);
        }

        for(int i = 0; i < getNumRows(); i++ ){
            for(int j=0; j < getNumCols(); j++){
                setGrid(i,j,new GamePiece());
            }
        }

        //add brambles
        for(int i = 0; i < getNumRows(); i++ ){
            for(int j=0; j < getNumCols(); j++){
                if ((i < 2 || i > 5)&& (j < 2 || j > 5)){
                    setGrid(i,j,new Bramble());
                }
            }
        }
        //set grids if player chooses to use 20 Zhens
        if (getNumZhens() == 20) {
            setGrid(3,0,new Zhen());
            setGrid(3,7,new Zhen());
            setGrid(4,0,new Zhen());
            setGrid(4,7,new Zhen());

            getGrid(3,0).setRowPos(3);
            getGrid(3,7).setRowPos(3);
            getGrid(4,0).setRowPos(4);
            getGrid(4,7).setRowPos(4);

            getGrid(3,0).setColPos(0);
            getGrid(3,0).setColPos(7);
            getGrid(3,0).setColPos(0);
            getGrid(3,0).setColPos(7);
        }

        //set grids if player chooses to use 18 Zhens
        if (getNumZhens() == 18) {
            setGrid(3,0, new Zhen());
            setGrid(3,7, new Zhen());

            getGrid(3,0).setRowPos(3);
            getGrid(3,7).setRowPos(3);

            getGrid(3,0).setColPos(0);
            getGrid(3,0).setColPos(7);
        }


        int counter = 0;
        //set grids if player chooses to use 16 Zhens
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumCols(); j++)
                if (grid[i][j].getSymbol() != '*' && counter < 16) {
                    grid[i][j] = new Zhen();
                    getGrid(i,j).setRowPos(i);
                    getGrid(i,j).setColPos(j);
                    counter++;
                }
        }

        setGrid(4,3,h1);
        setGrid(4,4,h2);
    }

    void displayGrid(){
        String dots = " ... ... ... ... ... ... ... ...";
        System.out.println(dots);
        for (int i = 0; i < getNumRows(); i++) {
            for (int j = 0; j < getNumCols(); j++) {
                if (j == 0) {
                    System.out.print("|");
                }
                if (grid[i][j].getSymbol() == '?') {
                    System.out.print("   |");
                } else {
                    System.out.print(" " + grid[i][j].getSymbol() + " |");
                }
                if (j == getNumCols()-1) {
                    System.out.println();
                    System.out.println(dots);
                }
            }
        }

        int e1 = 0;
        int e2 = 0;
        if (getGrid(getHunter1Coord()[0],getHunter1Coord()[1]) instanceof Hunter) {
            e1 = ((Hunter) getGrid(getHunter1Coord()[0],getHunter1Coord()[1])).getEnergyLevel();
        }

        if (getGrid(getHunter2Coord()[0],getHunter2Coord()[1]) instanceof Hunter) {
            e2 = ((Hunter) getGrid(getHunter2Coord()[0],getHunter2Coord()[1])).getEnergyLevel();
        }


        //displays Hunter F energy
        if (getGrid(getHunter1Coord()[0],getHunter1Coord()[1]) instanceof Hunter) {
            System.out.println("Hunter " + getGrid(getHunter1Coord()[0], getHunter1Coord()[1]).getSymbol() + " energy: " + e1);
        }
        //displays Hunter B energy
        if (getGrid(getHunter2Coord()[0],getHunter2Coord()[1]) instanceof Hunter) {
            System.out.println("Hunter " + getGrid(getHunter2Coord()[0], getHunter2Coord()[1]).getSymbol() + " energy: " + e2);
        }
        //System.out.println("Number of Hunters "+getNumHunters());
        System.out.println("Number of Zhens "+getNumZhens());
    }

    public void displayMovementOptions (int player)
    {
        if (player == 1) //Hunter
        {
            System.out.println(" 1(North),  2(South),  3(East),  4(West) ");
            System.out.println(" 5(North-west),  6(South-west),  7(South-East),  8(North-east) ");
            System.out.println(" 9(Eat North),  10(Eat West),  11(Eat South),  12(Eat East)");
            System.out.println(" ");
        }
        else if (player == 2) //Zhen
        {
            System.out.println(" 1(North),  2(South),  3(East),  4(West) ");
            System.out.println(" 5(North-west),  6(South-west),  7(South-East),  8(North-east) ");
            System.out.println(" ");
        }
    }//displayMovementOptions
}