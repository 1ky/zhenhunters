public class Bramble extends GamePiece {
    public Bramble(){
        setType("bramble");
        setSymbol('*');
    }
}
