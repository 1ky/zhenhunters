public class GamePiece {
    private String type;
    private char symbol;
    private int rowPos;
    private int colPos;

    public GamePiece() {
        setType("?");
        setSymbol('?');
    }

    public void setType(String ty) {
        type = ty;
    }

    public void setSymbol(char sym) {
        symbol = sym;
    }

    public void setRowPos(int rows) {
        rowPos = rows;
    }

    public void setColPos(int col) {
        colPos = col;
    }

    public String getType() {
        return type;
    }

    public int getColPos() {
        return colPos;
    }

    public int getRowPos() {
        return rowPos;
    }

    public char getSymbol() {
        return symbol;
    }
}
