public interface EatingActions
{
    void eatWest();
    void eatEast();
    void eatNorth();
    void eatSouth();
}